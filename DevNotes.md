
## User Agent:

'Mozilla/5.0 (compatible; MSIE 8.0; Windows NT 6.0; Trident/4.0; Acoo Browser 1.98.744; .NET CLR 3.5.30729)'

## Proxy

* https://github.com/pantuts/crag/

To configure a proxy, use the Mechanize#set_proxy method. It takes four
parameters, the proxy host, port and an optional user and password. In
this example, a proxy at localhost on port 10000 is configured in the
block passed to Mechanize#new.

    agent = Mechanize.new do|a|
        a.set_proxy('localhost', 10000, 'user', 'pass')
        a.user_agent_alias = "Windows IE 6"
    end

## API For Common Data Source

* Amazon
* Ebay
* Craiglist
* Expedia
* Trivago
* Google
