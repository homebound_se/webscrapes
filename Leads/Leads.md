# Leads Generation Document

## Upwork Web Scraping Jobs

### 07-16-2015

* [Web Crawling, Scraping, API Intergrations, XML Feeds](https://www.upwork.com/jobs/Web-Crawling-Scraping-API-Intergrations-XML-Feeds_~01baaa58e6a76f16f8)
  http://websta.me/tag/datpiff?lang=en
* [Scrapy python scraper needed for http://www.grohe.com/us](https://www.upwork.com/jobs/Scrapy-python-scraper-needed-for-http-www-grohe-com_~014b3bd04f7d28e681?saved_jobs=1)
* [Api integration](https://www.upwork.com/jobs/Api-integration_~010275b26c85cb6624?saved_jobs=1)

### 07-09-2015

* [Quick and Easy Web Scraping for Programmer. Collect data from government website, record into Excel or Open Office Spreadsheet.](https://www.upwork.com/jobs/Quick-and-Easy-Web-Scraping-for-Programmer-Collect-data-from-government-website-record-into-Excel-Open-Office-Spreadsheet_~01fae217399a10a685)

### 07-08-2015

* [Use Scrapy to extract website data into csv file](https://www.upwork.com/jobs/Use-Scrapy-extract-website-data-into-csv-file_~0132a6c72024003094)
* [Custom Scraping Specialist with skills in Advanced Excel, Python, HTML](https://www.upwork.com/jobs/Custom-Scraping-Specialist-with-skills-Advanced-Excel-Python-HTML_~01350e70af73d4ed4c)


## [Freelancer.com Web Scraping Jobs](https://www.freelancer.com/jobs/Web-Scraping/)

### 07-08-2015

* [Data Entry](https://www.freelancer.com/projects/Data-Entry/Data-Entry-8028194/)
* [Upload batches of between 100 and 1000 products to our website](https://www.freelancer.com/projects/Data-Entry/Upload-batches-between-products-our/)
