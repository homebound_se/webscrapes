#
# Author: Alvin Difuntorum <alvinpd09@gmail.com>
#
#
#

require 'mechanize'
require 'logger'
require 'nokogiri'

def test_proxy(chkaddr, proxy, port)
  puts "Testing Proxy: #{proxy}:#{port}..."
  puts "    Downloading page..."

  agent = Mechanize.new do |a|
    a.log = Logger.new(STDERR)
    a.set_proxy(proxy, port)
    a.user_agent_alias = 'Mac Safari'
  end

  begin
    html = agent.get(chkaddr).body
  rescue Exception => e
    puts "Error: #{e}"
    return false
  else
    puts "       Done..."
  ensure

  end

  puts "    Processing page..."
  doc = Nokogiri::HTML(html)
  nodes = doc.xpath("//div[@id='location']/h3")
  my_ip = nodes[0].text.sub(/.*\:/,'').strip

  puts "    IP: #{my_ip}"

  sleep 2.0 + rand

  agent.shutdown
end

def get_proxy_list
  html = ""
  url = "http://bestproxylist.com/ip-address-servers/"
  fname = "data/proxy_source.html"

  agent = Mechanize.new do |a|
    # a.log = Logger.new(STDERR)
    a.user_agent_alias = 'Mac Safari'
  end

  # Check if the file is available and download it if not
  unless File.exists?(fname)
    begin
      page = agent.get(url)
    rescue Exception => e
      puts "Error: #{e}"
      exit
    else
      page.save(fname)
    ensure
      sleep 1
    end
  end

  doc = Nokogiri::HTML(File.open(fname))

  nodes = doc.xpath("//div[@class='entry-content']/table/tbody/tr")
  nodes.each do |n|
    proxy = n.xpath("td")
    next if proxy[0].text.include? "IP Address"

    yield proxy[0].text, proxy[1].text.to_i
  end
end

get_proxy_list do |proxy, port|
  test_proxy("http://www.ip-adress.com/", proxy, port)
  puts
end

# End of file
