#
# Alvin Difuntorum <alvinpd09@gmail.com>
#
# https://www.freelancer.com/jobs/Web-Scraping/Scraper/
#
# Project Description:
#
# Looking for a data scraper to be made.
#
# The scraper has to take key data from CharityNavigator.org and export
# it into an Excel spreadsheet.
#
# It has to go through every single charity on the site. Currently
# there's about 7,000 charities. Here's a directory of all those
# charities (click through alphabetically for each individual one):
# http://www.charitynavigator.org/index.cfm?bay=search.alpha
#
# The scraper will go to each individual charity page, such as this:
# http://www.charitynavigator.org/index.cfm?bay=search.summary&orgid=6127#.VXHl01xViko
#
# And from there it will pull the following data:
#
# Name of Charity
# Street Address
# City, State, Zip Code
# Phone Number
# Board Leadership
# CEO
#
# Financial Score (out of 100)
# Accountability and Transparency Score (out of 100)
#
# After that, it will scrape:
# Every single line of the INCOME STATEMENT.
#
#
# That's all the data that I need. The data then needs to be formatted
# into an excel document.
#
# Preferably I'll be able to run this program on a Mac.
#
#
# Best,
# Max
#
# Desired Skills MySQL Administration, .NET Framework, PHP, Python
#

require 'csv'
require 'nokogiri'
require 'mechanize'

class CharityData
  def initialize(orgid, url)
    @url = url
    @orgid = orgid
    @filename = "data/summary_#{orgid}.html"
    @invalid = false

    summary
  end

  def summary
    doc = Nokogiri::HTML(File.open(@filename))

    # Retrieve Charity Information

    ## Retrieve address and contact information
    xpath_base = "//div[@id='leftnavcontent']/div[@class='rating']/p[1]"

    @ch_name = doc.xpath("#{xpath_base}/strong").text

    # Retrieve address of the Charity
    nodes = doc.xpath("#{xpath_base}/text()")

    @invalid = true if nodes.length == 0
    return if @invalid

    puts "#{@ch_name}: nodes=#{nodes.length}"

    @ch_strt = nodes[0].to_s.strip!

    if (nodes.length == 6 and !nodes[3].to_s.include? 'tel') or
        nodes.length == 5
      @ch_suite = ""
      @ch_city = nodes[1].to_s.strip!
      @ch_phone = nodes[2].to_s.strip!
    else
      @ch_suite = nodes[1].to_s.strip!
      @ch_city = nodes[2].to_s.strip!
      @ch_phone = nodes[3].to_s.strip!
    end

    ## Retrieve Board Leadership & CEO information
    xpath_base = "//div[@id='leftnavcontent']/div[3]/p[1]/strong"
    @ch_chair = doc.xpath(xpath_base).text

    xpath_base = "//div[@id='leftnavcontent']/div[3]/p[2]/strong"
    @ch_ceo = doc.xpath(xpath_base).text

    ## Retrieve Financial & Accountability and Transparency Scores
    xpath_base = "//div[@id='summary']//table[1]//table[1]/tr[3]/td[2]"
    @ch_finsc = doc.xpath(xpath_base).text.to_f

    xpath_base = "//div[@id='summary']//table[1]//table[1]/tr[4]/td[2]"
    @ch_acct = doc.xpath(xpath_base).text.to_f

    ## Scrape the Income Statement
    xpath_base = "//div[@id='summary']//div[@class='summaryBox'][2]//table[1]/tr"
    nodes = doc.xpath(xpath_base)

    @invalid = true if nodes.empty?
    return if @invalid

    @ch_contrib_co = nodes[2].xpath('td[2]').text.gsub(/[^\d\.-]/,'').to_f
    @ch_contrib_fc = nodes[3].xpath('td[2]').text.gsub(/[^\d\.-]/,'').to_f
    @ch_contrib_md = nodes[4].xpath('td[2]').text.gsub(/[^\d\.-]/,'').to_f
    @ch_contrib_fe = nodes[5].xpath('td[2]').text.gsub(/[^\d\.-]/,'').to_f
    @ch_contrib_ro = nodes[6].xpath('td[2]').text.gsub(/[^\d\.-]/,'').to_f
    @ch_contrib_gg = nodes[7].xpath('td[2]').text.gsub(/[^\d\.-]/,'').to_f
    @ch_tc  = nodes[8].xpath('td[2]').text.gsub(/[^\d\.-]/,'').to_f
    @ch_psr = nodes[9].xpath('td[2]').text.gsub(/[^\d\.-]/,'').to_f
    @ch_tpr = nodes[10].xpath('td[2]').text.gsub(/[^\d\.-]/,'').to_f
    @ch_or  = nodes[11].xpath('td[2]').text.gsub(/[^\d\.-]/,'').to_f
    @ch_tr  = nodes[12].xpath('td[2]').text.gsub(/[^\d\.-]/,'').to_f
    @ch_pe  = nodes[15].xpath('td[2]').text.gsub(/[^\d\.-]/,'').to_f
    @ch_ae  = nodes[16].xpath('td[2]').text.gsub(/[^\d\.-]/,'').to_f
    @ch_fe  = nodes[17].xpath('td[2]').text.gsub(/[^\d\.-]/,'').to_f
    @ch_tfe = nodes[18].xpath('td[2]').text.gsub(/[^\d\.-]/,'').to_f

    @ch_pta = nodes[20].xpath('td[2]').text.gsub(/[^\d\.-]/,'').to_f
    @ch_ed  = nodes[21].xpath('td[2]').text.gsub(/[^\d\.-]/,'').to_f
    @ch_na  = nodes[23].xpath('td[2]').text.gsub(/[^\d\.-]/,'').to_f
  end

  def to_s
    ## Show Scraped Charity information

    return "Invalid Charity: #{@orgid} #{@url}" if @invalid

    "\tCharity Name: #{@ch_name}\n" \
    "\tStreet: #{@ch_strt} #{@ch_suite}\n" \
    "\tState: #{@ch_city}\n" \
    "\tPhone: #{@ch_phone.gsub('tel: ', '')}\n" \
    "\tChair: #{@ch_chair}\n" \
    "\tCEO: #{@ch_ceo}\n" \
    "\tFinancial Score: #{@ch_finsc}\n" \
    "\tAccountability & Transparency: #{@ch_acct}\n" \
    "\tIncome Statement\n" \
    "\tContributions\n" \
    "\t  Contributions, Gifts & Grants: #{@ch_contrib_co}\n" \
    "\t  Federated Campaigns: #{@ch_contrib_fc}\n" \
    "\t  Membership Dues: #{@ch_contrib_md}\n" \
    "\t  Fundraising Events: #{@ch_contrib_fe}\n" \
    "\t  Related Organizations: #{@ch_contrib_ro}\n" \
    "\t  Government Grants: #{@ch_contrib_gg}\n" \
    "\tTotal Contributions: #{@ch_tc}\n" \
    "\t  Program Service Revenue: #{@ch_psr}\n" \
    "\tTotal Primary Revenue: #{@ch_tpr}\n" \
    "\t  Other Revenue: #{@ch_or}\n" \
    "\tTotal Revenue: #{@ch_tr}\n" \
    "\tEXPENSES\n" \
    "\t  Program Expenses: #{@ch_pe}\n" \
    "\t  Administrative Expenses: #{@ch_ae}\n" \
    "\t  Fundraising Expenses: #{@ch_fe}\n" \
    "\tTOTAL FUNCTIONAL EXPENSES: #{@ch_tfe}\n" \
    "\tPayments to Affiliates: #{@ch_pta}\n" \
    "\tExcess (or Deficit) for the year: #{@ch_ed}\n" \
    "\tNet Assets: #{@ch_na}\n"
  end

  def to_array
    return [ @orgid, @ch_name, @url ] if @invalid

    [ @orgid,
      @ch_name,
      @ch_strt,
      @ch_suite,
      @ch_city,
      @ch_phone.gsub('tel: ', '').gsub('fax: ', ''),
      @ch_chair,
      @ch_ceo,
      @ch_finsc,
      @ch_acct,
      @ch_contrib_co,
      @ch_contrib_fc,
      @ch_contrib_md,
      @ch_contrib_fe,
      @ch_contrib_ro,
      @ch_contrib_gg,
      @ch_tc,
      @ch_psr,
      @ch_tpr,
      @ch_or,
      @ch_tr,
      @ch_pe,
      @ch_ae,
      @ch_fe,
      @ch_tfe,
      @ch_pta,
      @ch_ed,
      @ch_na
    ]
  end

  def self.header
    [ "Id",
      "Name",
      "Street",
      "Suite",
      "City",
      "Phone",
      "Chairman",
      "CEO",
      "Financial Score",
      "Accountability & Transparency",
      "Contributions, Gifts & Grants",
      "Federated Campaigns",
      "Membership Dues",
      "Fundraising Events",
      "Related Organizations",
      "Government Grants",
      "Total Contributions",
      "Program Service Revenue",
      "Total Primary Revenue",
      "Other Revenue",
      "Total Revenue",
      "Program Expenses",
      "Administrative Expenses",
      "Fundraising Expenses",
      "Total Functional Expenses",
      "Payments to Affiliates",
      "Exces (or Deficit) for the year",
      "Net Assets"
    ]
  end
end

agent = Mechanize.new { |agent|
  agent.user_agent_alias = 'Mac Safari'
}

## Retrieve A-Z listing page
AZ_LISTING = 'http://www.charitynavigator.org/index.cfm?bay=search.alpha'
puts "Downloading listing page: #{AZ_LISTING}"
page = agent.get(AZ_LISTING)

csv = CSV.open("data/CharityIndex.csv", "wb")

csv << CharityData.header

# Download individual alphabetical listing page. Perform web scraping
# procedure to each individual charity information page.
page.links.each do |link|
  next if link.href == nil
  next if not link.href.include? 'bay=search.alpha&ltr='

  filename = "data/page_#{link.href.chars.last}.html"

  unless File.exists?(filename)
    puts "Fetching remote #{link.href}..."

    begin
      page = agent.get(link.href)
    rescue Exception => e
      puts "Error: #{e}"
      sleep 3
      next
    else
      page.save(filename)
      puts "\t...Success, saved to #{filename}"
    ensure
      sleep 1.0 + rand
    end
  end

  # This page contains the list of Charities for each alphanumeric value
  puts "Scraping data from page #{link.href.chars.last}..."

  # Parse the page and retrieve each page of the charities
  doc = Nokogiri::HTML(File.open(filename))

  rows = doc.xpath("//a")
  rows.each do |row|
    next if row['href'] == nil
    next if not row['href'].include? 'orgid='

    orgid = row['href'].scan( /\d+$/ ).first
    puts "#{row.text} (#{orgid})"

    filename = "data/summary_#{orgid}.html"

    unless File.exists?(filename)
      puts "\tFetching remote #{row['href']}..."

      begin
        page = agent.get(row['href'])
      rescue Exception => e
        puts "Error: #{e}"
        sleep 3
        next
      else
        page.save(filename)
        puts "\t...Success, saved to #{filename}"
      ensure
        sleep 2.0 + rand
      end
    end # unless File.exists?

    # Parse and retrieve information from the summary page.
    ch = CharityData.new(orgid, row['href'])
    puts ch
    csv << ch.to_array

    puts
  end # rows.each

end

# End of file
