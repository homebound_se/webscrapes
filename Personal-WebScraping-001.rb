# Personal-WebScraping-001.rb
#
# Alvin Difuntorum <alvinpd09@gmail.com>
#
# Extract movie information and torrent file from YTS website
#
#

require 'rubygems'
require 'mechanize'
require 'nokogiri'

# Directory Index: http://yts.re/browse-movies/0/1080p/all/0/latest

DIRECTORY_INDEX = 'http://yts.re/browse-movies/0/1080p/all/0/latest'

agent = Mechanize.new { |agent|
	agent.user_agent_alias = 'Mac Safari'
}

page = agent.get(DIRECTORY_INDEX)

# Retrieve the last page number. We'll use it to navigate through the
# paginated list.

last = page.search("//ul/li[last()-3]")[1].text.to_i

# Create data directory if it doesn't exist yet
Dir.mkdir 'data' if not File.directory? 'data'

1.upto(last) do |pageno|
	url = "http://yts.re/browse-movies/0/1080p/all/0/latest?page=#{pageno}"
	fname = "data/yts_page_#{pageno}.html"

	## Download the html file if it does not exist yet
	unless File.exists?(fname)
		puts "Fetching remote #{url}..."

		begin
			page = agent.get(url)
		rescue Exception => e
			puts "Error: #{e}"
			sleep 3
			next
		else
			page.save(fname)
			puts "\t...Success, saved to #{fname}"
		ensure
			sleep 1.0 + rand
		end
  	end

	# break ## Debug
end
