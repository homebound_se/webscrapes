#
# Alvin Difuntorum <alvinpd09@gmail.com>
#
# https://www.upwork.com/jobs/Script-for-website-data_~016c4d709627f01573
#
# Project Description:
#
# I need a script that scrapes data from a publicly available website. I have
# provided the specifics in the following Google spreadsheet:
#
# https://docs.google.com/spreadsheets/d/1v8KYqcNG8ew0p_43cNR7EVSbzTuBYWEwh_h96sTGEBk/edit#gid=0
#
# A few notes to explain what I need:
#
# - The script should generate a list of records as specified in row 1
# - I have provided an explanation of each field in row 2, and an example
#   of how a record should look in row 3 (taken from the state of Iowa / IA)
# - I need CSV exports of records for ALL 50 STATES; the master list for
#   each state is listed in B6-B55
# - Because some lists are very long, you can limit them to recipients
#   (farms) with > $1,000,000 in subsidies (Column H)
# - The name of each recipient (Column C) should not include the " ∗" at
#   the end
# - The work needs to be completed in the next 1 day (or maximum 2 days);
#   I will give preference to freelancers who can work on this right away
#
#

require 'csv'
require 'mechanize'
require 'nokogiri'

SLEEP_BASE = 5.0
WEBSITE_BASE = "http://farm.ewg.org"

def parse_farm_information(agent, url)
  puts "    Scraping data for farm #{url.sub(/.*\=/, '')}..."

  crops = []
  fname = "data/farm_cust_#{url.sub(/.*\=/, '')}.html"

  # Check if the file is available and download it if not
  unless File.exists?(fname)
    begin
      page = agent.get(url)
    rescue Exception => e
      puts "Error: #{e}"
      return crops
    else
      page.save(fname)
    ensure
      sleep SLEEP_BASE + rand
    end
  end

  doc = Nokogiri::HTML(File.open(fname))

  nodes = doc.xpath("//table[@id='crop_table']/tr")
  nodes.each do |n|
    next if n.to_s.include? 'class="row1"'

    crops << n.xpath('td[1]').text.sub(/\ .*/, '').downcase
  end

  return crops
end

# Download each page of the region if it is not yet downloaded. Parse each
# page and retrieve the necessary information.
def parse_state_page_collection(fips, region)
  idx = 0
  last = 0
  master = "#{WEBSITE_BASE}/top_recips.php?fips=#{fips}&progcode=totalfarm&regionname=#{region}"
  agent = Mechanize.new do |a|
    a.user_agent_alias = 'Mac Safari'
  end

  loop do
    puts "Scraping data for #{region}: page #{idx}..."

    url = "#{WEBSITE_BASE}/top_recips.php?fips=#{fips}&progcode=totalfarm&page=#{idx}"
    fname = "data/farm_#{fips}_page#{idx}.html"

    # Check if the file is available and download it if not
    unless File.exists?(fname)
      begin
        page = agent.get(url)
      rescue Exception => e
        puts "Error: #{e}"
        exit
      else
        page.save(fname)
      ensure
        sleep SLEEP_BASE + rand
      end
    end

    # Create an HTML document model for parsing the HTML file
    doc = Nokogiri::HTML(File.open(fname))

    # Retrieve the farm list information from this page
    nodes = doc.xpath("//table[@id='top_recip_table']/tr")

    nodes.each do |n|
      next if n.to_s.include? 'class="row1"'
      td = n.xpath("td")

      rank = td[0].text
      recipient = td[1].text.sub(/\ .*/, '').strip
      rurl = "#{WEBSITE_BASE}/#{td[1].xpath('a')[0]['href']}"
      city = td[2].text.sub(/\,.*/, '')
      state = td[2].text.sub(/.*\,/, '').gsub(/\d/, '').strip
      zip = td[2].text.gsub(/[^\d\.]/, '')
      total = td[3].text.gsub(/[^\d\.-]/, '').to_i

      # Stop if the total subsidy is already less than $1,000,000
      return if total < 1000000

      # Parse the farm page to retrieve the crops
      crops = parse_farm_information(agent, rurl)

      # Rank, Recipient, URL, City, State, ZIP, Total Farming Subsidies, Crops
      yield master, rank, recipient, rurl, city, state, zip, total,
            crops.to_s.gsub(/\"/, '').sub(/\[/, '').sub(/\]/, '')

      # break ## DEBUG
    end

    # If idx == 0, parse the total pages to determine the last page of
    # the pagination. Another way is to check if the 'Next >>' anchor
    # is available or not.
    if idx == 0
      element = doc.xpath("//table[@id='totalbox']//h5/span[@class='highlight'][3]")
      last = element.text.gsub(/[^\d\.-]/, '').to_i / 20
    end

    # We finished parsing all the pages of this region
    break if idx == last

    # Proceed to the next page
    idx = idx + 1

    # break ## DEBUG
  end # loop do

end

def us_states
  [
    ['01000', 'Alabama', 'AL'],
    ['02000', 'Alaska', 'AK'],
    ['04000', 'Arizona', 'AZ'],
    ['05000', 'Arkansas', 'AR'],
    ['06000', 'California', 'CA'],
    ['08000', 'Colorado', 'CO'],
    ['09000', 'Connecticut', 'CT'],
    ['10000', 'Delaware', 'DE'],
    ['12000', 'Florida', 'FL'],
    ['13000', 'Georgia', 'GA'],
    ['15000', 'Hawaii', 'HI'],
    ['16000', 'Idaho', 'ID'],
    ['17000', 'Illinois', 'IL'],
    ['18000', 'Indiana', 'IN'],
    ['19000', 'Iowa', 'IA'],
    ['20000', 'Kansas', 'KS'],
    ['21000', 'Kentucky', 'KY'],
    ['22000', 'Louisiana', 'LA'],
    ['23000', 'Maine', 'ME'],
    ['24000', 'Maryland', 'MD'],
    ['25000', 'Massachusetts', 'MA'],
    ['26000', 'Michigan', 'MI'],
    ['27000', 'Minnesota', 'MN'],
    ['28000', 'Mississippi', 'MS'],
    ['29000', 'Missouri', 'MO'],
    ['30000', 'Montana', 'MT'],
    ['31000', 'Nebraska', 'NE'],
    ['32000', 'Nevada', 'NV'],
    ['33000', 'New Hampshire', 'NH'],
    ['34000', 'New Jersey', 'NJ'],
    ['35000', 'New Mexico', 'NM'],
    ['36000', 'New York', 'NY'],
    ['37000', 'North Carolina', 'NC'],
    ['38000', 'North Dakota', 'ND'],
    ['39000', 'Ohio', 'OH'],
    ['40000', 'Oklahoma', 'OK'],
    ['41000', 'Oregon', 'OR'],
    ['42000', 'Pennsylvania', 'PA'],
    ['44000', 'Rhode Island', 'RI'],
    ['45000', 'South Carolina', 'SC'],
    ['46000', 'South Dakota', 'SD'],
    ['47000', 'Tennessee', 'TN'],
    ['48000', 'Texas', 'TX'],
    ['49000', 'Utah', 'UT'],
    ['50000', 'Vermont', 'VT'],
    ['51000', 'Virginia', 'VA'],
    ['53000', 'Washington', 'WA'],
    ['54000', 'West Virginia', 'WV'],
    ['55000', 'Wisconsin', 'WI'],
    ['56000', 'Wyoming', 'WY']
  ]
end

csv = CSV.open("data/FarmInformation.csv", "wb")

csv << [ "List URL", "Rank", "Recipient", "URL", "City", "State",
         "ZIP", "Total Farming Subsidies", "Crops" ]

us_states.each do |s|
  parse_state_page_collection(s[0], s[1].sub(/\ /, '')) do |master, rank, recp, url, city, state, zip, total, crops|
    csv << [ master, rank, recp, url, city, state, zip, total, "#{crops}" ]
  end
  # break ## DEBUG
end

# End of file
