#
# Alvin Difuntorum <alvinpd09@gmail.com>
#
# oDesk Link: https://www.odesk.com/jobs/Scrape-data-from-website_~01c002e6dc91bf3e5e
#
# Job Description:
# I need the data from this site scraped into a csv file.
#
# http://services.siaonline.org/eBusiness/Directory/Index.aspx
#
# When you click on each company, a pop over of the company is presented with
# the company's info.
#
# Collect all the info according to the columns. They coordinate with the
# headers of each section.
#
# Company Name
# Street Address
# City
# State Zip
# Website
# Phone
# Primary Contact Name
# Organization Description
# Industry Segment
# Markets Served
# Year Established
# Staff Size
# Global Regions Served
# Key Personnel (one column for each person)
# Branch Offices
#
# Please send a sample of a few records to prove that you can do this job.
# I am looking to hire and get this job done asap.
#
# Thank You!
#
# --Ryan
#

require 'csv'
require 'nokogiri'
require 'mechanize'

## This function handles a row of data from the table row selector
def row_data(row, data)
  website_saved = false

  ## Website URL
  a = row.at_css 'td a'
  puts "#{a['href']}" if not a.nil?

  spans = row.css('td span')

  spans.each do |s|
    ## Unneeded data
    next if s['id'].eql? 'lblDetails'

    ## Company Name
    puts "Company Name:\n    #{s.text}" if s['id'].eql? 'lblFullName'
    data << s.text if s['id'].eql? 'lblFullName'

    ## Website
    data << a['href'] if (not a.nil? and not website_saved)
    website_saved = true

    ## Address -> Street Address, City, State, Zip
    if s['id'].eql? 'lblAddressBlock'
      data << s.search("//span[@id='lblAddressBlock']/text()[1]").text
      data << s.search("//span[@id='lblAddressBlock']/text()[2]").text
      data << s.search("//span[@id='lblAddressBlock']/text()[3]").text
      data << s.search("//span[@id='lblAddressBlock']/text()[4]").text
    end

    ## Phone Number
    puts "Phone Number:\n    #{s.text}" if s['id'].eql? 'lblBusinessPhone'
    data << s.text if s['id'].eql? 'lblBusinessPhone'

    ## Primary Contact
    puts "Primary Contact:\n    #{s.text}" if s['id'].eql? 'txtPrimaryContact'
    data << s.text if s['id'].eql? 'txtPrimaryContact'

    ## Organization Description
    data << s.text if s['id'].eql? 'txtOrgDesc'

    ## Industry Segment see #row_industry
    ## Markets Served see #row_market

    ## Year Established
    data << "#{s.text}" if s['id'].eql? 'txtYearEst'

    ## Staff Size
    data << "#{s.text}" if s['id'].eql? 'txtStaffSize'

    ## Global Regions Served #row_region
    ## Key Personnel see #row_personnel

    ## Branch Offices
  end
end

def row_data_list(row, pattern)
  spans = row.css('td span')

  spans.each do |s|
    yield(s.text) if s['id'].include? pattern
  end
end

def row_personnel(row)
  f_name = ""
  m_name = ""
  l_name = ""

  spans = row.css('td span')

  spans.each do |s|
    f_name = s.text if s['id'].include? 'lblFirstName'
    m_name = s.text if s['id'].include? 'lblMiddleName'
    l_name = s.text if s['id'].include? 'lblLastName'
  end

  if m_name.empty?
    yield("#{f_name} #{l_name}")
  else
    yield("#{f_name} #{m_name} #{l_name}")
  end
end

## CSV initializion and directory index data

csv = CSV.open("data/DirectoryIndex.csv", "wb")

## Perform download and extraction of necessary data

agent = Mechanize.new { |agent|
  agent.user_agent_alias = 'Mac Safari'
}

## Retrieve the business directory page
DIRECTORY_INDEX = "http://services.siaonline.org/eBusiness/Directory/Index.aspx"
puts "Loading website: #{DIRECTORY_INDEX}"
page = agent.get(DIRECTORY_INDEX)

puts "Loading Directory Index..."
form = page.form('aspnetForm')
form.__EVENTTARGET = 'ctl00$MainContent$lnkbtnQuickList'
form.__EVENTARGUMENT = ''

## Retrieve the page with all the listings
page = agent.submit(form)

puts "Extracting Business Information..."

page.links.each do |link|
  next if link.href == nil
  next if not link.href.include? 'javascript:openPopup'

  id = link.href.match('\d+')
  fname = "data/#{id}.html"
  url = "http://services.siaonline.org/eBusiness/Directory/DirectoryPopUp.aspx?id=#{id}"

  ## Download the html file if it does not exist yet
  unless File.exists?(fname)
    puts "Fetching remote #{url}..."

    begin
      page = agent.get(url)
    rescue Exception => e
      puts "Error: #{e}"
      sleep 3
      next
    else
      page.save(fname)
      puts "\t...Success, saved to #{fname}"
    ensure
      sleep 1.0 + rand
    end
  end

  str = []
  data = []

  puts "\nScraping data with id #{id}"

  # Parse the downloaded HTML files and retrieve the following:
  doc = Nokogiri::HTML(File.open(fname))

  rows = doc.xpath('//table/tr')
  rows.each do |row|
    row_data(row, data)
  end

  {"//table[@id='dlstIndustrySegments']/tr" => 'dlstIndustrySegments_ctl',
   "//table[@id='dlstMarketsServed']/tr" => 'dlstMarketsServed_ctl',
   "//table[@id='dlstGlobalRegionsServed']/tr" => 'dlstGlobalRegionsServed_ctl'}.each do |xpath, pattern|
    rows = doc.xpath(xpath)
    rows.each do |row|
      row_data_list(row, pattern) { |s| str << s }
    end

      data << str.join(", ")
      str.clear
  end

  rows = doc.xpath("//table[@id='dlstKeyPersonnel']/tr")
  rows.each do |row|
    row_personnel(row) { |s| data << s }
  end

  csv << data
end
