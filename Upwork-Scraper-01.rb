#
# Alvin Difuntorum <alvinpd09@gmail.com>
# Client: http://polisci2.ucsd.edu/jfreeves/
#
# https://www.upwork.com/jobs/Quick-and-Easy-Web-Scraping-for-Programmer-Collect-data-from-government-website-record-into-Excel-Open-Office-Spreadsheet_~01fae217399a10a685
#
# Project Description:
#
# This task involves scraping data from a Japanese government website
# and organizing it into 2 excel or open-office spreadsheets.  *Please
# note* : I am not interested in hiring anyone for a manual copy-and-paste
# job, as that will take far too long.  The data will need to be scraped
# using grepping methods or specialized software (which is why I've listed
# Python as a needed skill).
#
# It is slightly more cumbersome than your typical web scraping task since
# the data sources for each variable/column in the excel spreadsheets come
# from multiple pages within the website… and of course, it’s in Japanese.
#
# That said, I will provide detailed instructions on the exact data I’m
# looking for with links to each data source, screen shots of the display,
# and a copy of the html source script for each variable.  You won’t need
# to be able to read Japanese to do it.  I'll also include an example of
# what each excel spreadsheet needs to look like, pasted at the end of
# the instructions.
#
# To make it more intuitive I will also describe what the data is for,
# and what, in English, the Japanese characters are referring to.
#
# If you’re interested please take a look at the instructions attached
# in the word document for this job posting.
#
# You can ascertain for yourself how much work you think it would be.
# Then get back to me with an estimate. If you can successfully complete
# this task relatively quickly, I will be able to hire you again for
# follow-up jobs that are nearly identical and for which you will be
# able to reuse much of the same coding.
#
# Open Attachment
#
# Skills Required: JavaScript Microsoft Excel Python Web scraping
#
# Dev Notes:
#
# http://support.mobileapptracking.com/entries/27347804-How-To-Import-a-Unicode-CSV-to-Excel
#

require 'csv'
require 'sequel'
require 'nokogiri'
require 'mechanize'

# Parse the vote and the name of the legislator
def legislator_votes_parse(rows)
  fname = ""
  lname = ""

  rows.each do |row|
    cols = row.xpath("td")
    next if cols.length != 9
    next if cols[0].to_s.include? '<tt>'

    next if cols[2].text.sub(/\S/, '').empty?

    sansei = cols[0].to_s.include? 'sansei' # Pro
    spacer = cols[1].to_s.include? 'hantai' # Con
    name   = cols[2].text # Name

    if name.match(/　/).to_s.length == 0
      fname = ""
      lname = name
    else
      fname = name.sub(/.*\　/, '')
      lname = name.sub(/\　.*/,'')
    end

    yield sansei, spacer, fname, lname

    next if cols[5].text.sub(/\S/, '').empty?

    sansei = cols[3].to_s.include? 'sansei' # Pro
    spacer = cols[4].to_s.include? 'hantai' # Con
    name   = cols[5].text # Name

    if name.match(/　/).to_s.length == 0
      fname = ""
      lname = name
    else
      fname = name.sub(/.*\　/, '')
      lname = name.sub(/\　.*/,'')
    end

    yield sansei, spacer, fname, lname

    next if cols[8].text.sub(/\S/, '').empty?

    sansei = cols[6].to_s.include? 'sansei' # Pro
    spacer = cols[7].to_s.include? 'hantai' # Con
    name   = cols[8].text # Name

    if name.match(/　/).to_s.length == 0
      fname = ""
      lname = name
    else
      fname = name.sub(/.*\　/, '')
      lname = name.sub(/\　.*/,'')
    end

    yield sansei, spacer, fname, lname
  end
end

# Parse the content of each bill page
def bill_votes_parse(filename)
  doc = Nokogiri::HTML(File.open(filename))
  tables = doc.xpath("//div[@id='ContentsBox']/table")

  tables.each do |table|
    # Parse: party
    caption = table.xpath("caption[@class='party']/text()[1]")
    next if caption.length == 0

    party = caption.text.sub(/\(.*/,'').strip

    # Parse: legislator and votes
    legislator_votes_parse(table.xpath("tr")) do |pro, con, fname, lname|
      bill = filename.sub(/.*\//, '').sub(/\..*/, '')
      if pro
        yield bill, party, fname.strip, lname.strip, "sansei"
      elsif con
        yield bill, party, fname.strip, lname.strip, "hantai"
      else
        yield bill, party, fname.strip, lname.strip, "spacer"
      end
    end
  end # tables
end

# Parse the contents of the main bills registry page
def bill_page_parse(filename)
  doc = Nokogiri::HTML(File.open(filename))
  links = doc.xpath("//div[@class='list']/table/tr/td//a")

  id = 0
  links.each do |link|
    next if link['href'] == nil
    next if link['href'].empty?

    id = id + 1

    str = link['href'].sub(/-/,'[').sub(/-/,']').sub(/.*\[/,'').sub(/\].*/,'')
    date = Date.strptime(str, "%m%d")

    yield id, link['href'].sub(/.*\//, '').sub(/\..*/, ''), date, link.text
  end
end

# Download and save webpages
def retrieve_pages(base_url, base_page, id)
  filename = "data/#{base_page}"
  agent = Mechanize.new { |agent|
    agent.user_agent_alias = 'Mac Safari'
  }

  # Check if the base file is available and download it if not
  unless File.exists?(filename)
    begin
      page = agent.get("#{base_url}/#{base_page}")
    rescue Exception => e
      puts "Error: #{e}"
      exit
    else
      page.save(filename)
    ensure
      sleep 2.0 + rand
    end
  end # unless

  yield filename

  doc = Nokogiri::HTML(File.open(filename))
  nodes = doc.xpath("//div[@class='list']/table/tr/td//a")

  nodes.each do |link|
    next if link['href'] == nil
    next if not link['href'].include? "#{id}-"

    filename = "data/#{link['href']}"
    unless File.exists?(filename)
      begin
        page = agent.get("#{base_url}/#{link['href']}")
      rescue Exception => e
        puts "Error: #{e}"
        sleep 3
        next
      else
        page.save(filename)
      ensure
        sleep 2.0 + rand
      end

    end # unless

    yield filename
  end # nodes.each
end # retrieve_pages

BASE_NUM  = 189
BASE_URL  = "http://www.sangiin.go.jp/japanese/joho1/kousei/vote/#{BASE_NUM}"
BASE_PAGE = "vote_ind.htm"

# Database repository initialization
DB = Sequel.sqlite # memory database

DB.create_table :bills do
  primary_key :id
  String      :slug, :unique => true, :null => false
  Date        :date
  String      :desc # Bill description
end

DB.create_table :parties do
  primary_key :id
  String      :name, :unique => true, :null => false
end

DB.create_table :legislators do
  primary_key :id
  String      :fname
  String      :lname
  foreign_key :party_id, :parties # This legislator belongs to a party
end

DB.create_table :votes do
  primary_key :id
  foreign_key :bill_id, :bills
  foreign_key :legislator_id, :legislators
  String      :vote
end

bills = DB[:bills]
votes = DB[:votes]
parties = DB[:parties]
legislators = DB[:legislators]

retrieve_pages(BASE_URL, BASE_PAGE, BASE_NUM) do |filename|
  if filename.include? BASE_PAGE
    puts "Processing bills information page..."
    bill_page_parse(filename) do |id, slug, date, desc|
      bills.insert(:id => id, :slug => slug, :date => date, :desc => desc)
    end
  else
    puts "Processing bill: #{filename}..."
    bill_votes_parse(filename) do |bill, group, fname, lname, vote|
      # Find the bill id from the database
      bill_id = bills[:slug => bill][:id]

      # Find the party id, create if not available
      parties.insert(:name => group) if parties[:name => group].nil?
      party_id = parties[:name => group][:id]

      # Find the legislator id, create if not available
      if legislators.where(:fname => fname, :lname => lname).first.nil?
        legislators.insert(:fname => fname, :lname => lname, :party_id => party_id)
      end
      l_id = legislators.where(:fname => fname, :lname => lname).first[:id]

      votes.insert(:bill_id => bill_id, :legislator_id => l_id, :vote => vote)
    end
  end
end

# Create CSV Data files

vd = [ [ "Last_name", "First_name", "Party" ] ]

## Save the legislators name and party first
legislators.join(:parties, :id => :party_id).each do |r|
  vd << [ r[:lname], r[:fname], r[:name] ]
end

## Save the Bills List into this csv file
bill_csv = CSV.open("data/BillIndex.csv", "wb")
bill_csv << [ "Bill_ID", "Date", "Bill_name" ]

## Create a votes list from the bills and legislators
bills.each do |b|
  bill_csv << [ b[:id], b[:date], b[:desc] ]

  i = 0
  vd[i] << "Bill_#{b[:id]}"

  DB.select(:votes__vote).from(:votes)
    .join(:bills, :id => :votes__bill_id)
    .join(:legislators, :id => :votes__legislator_id)
    .join(:parties, :id => :legislators__party_id)
    .where(:bills__id => b[:id]).each do |r|

    i = i + 1
    vd[i] << r[:vote]
  end
end

## Save the Votes List into this csv file
vote_csv = CSV.open("data/VotesIndex.csv", "wb")

vd.each do |v|
  vote_csv << v
end

# Print Reports

puts "Total Bills: #{bills.count}"
puts "Total Votes: #{votes.count}"
puts "Total Parties: #{parties.count}"
puts "Total Legislators: #{legislators.count}"

# End of file
