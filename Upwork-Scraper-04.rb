#
# Alvin Difuntorum <alvinpd09@gmail.com>
#
# https://www.upwork.com/jobs/Data-Entry-List-Excel-Copy-from-Website-Directory_~014684e08ab8c7408b
#
# Project Description:
#
# See attached excel file for instructions and sample entries.
#
# Need to copy and paste basic membership information for each member from
# an online database into an Excel file.
#
# Any questions, please let me know.
#
# I need someone who can do this quickly, at an affordable price, and not
# make any mistakes.
#
# Sam
#
# Attachments:
#   https://www.upwork.com/att/~~nT9iPWRj*QVJSPGT27hyzGIEzOJ1iXxuAaUCxg451ZvENMosEGq5v5bjRSfB3vIb
#

require 'csv'
require 'sequel'
require 'nokogiri'
require 'mechanize'

# Login to the website with the username and password information.
def site_login(agent, username, password)
  url = "http://www.scholarshipproviders.org/Default.aspx"

  begin
    agent.get(url)
  rescue Exception => e
    puts "Exception: #{e}"
    return false
  else
    sleep 2 + rand
  end

  agent.page.forms[0]['ctl00$Header$LoginView2$Login1$UserName'] = username
  agent.page.forms[0]['ctl00$Header$LoginView2$Login1$Password'] = password

  begin
    login = agent.page.forms[0].button_with(
                  :name => 'ctl00$Header$LoginView2$Login1$LoginButton')
    agent.page.forms[0].click_button(login)
  rescue Exception => e
    puts "Exception: #{e}"
    return false
  else
    sleep 2 + rand
  end

  return true
end

# Scrape member organization information from the cached web page
def site_scrape_member(agent, db, idx_page)
  base = 'http://www.scholarshipproviders.org/secure'
  # puts "Processing Page: #{idx_page}" # DEBUG
  html = Nokogiri::HTML(File.open(idx_page))

  pfx = "ctl00_PublicContent_FormView1_"
  dtl = "ctl00_PublicContent_DataList1_ctl"

  html.xpath("//a").each do |a|
    next if a['href'].nil?
    next if not a['href'].include? 'member_details.aspx?OrgID='

    fname = "data/nspa_orgid_#{a['href'].gsub(/[^\d]/, '')}.html"
    url = "#{base}/#{a['href']}"

    unless File.exists?(fname)
      begin
        puts "  Downloading Membership Data: #{url}" # DEBUG
        agent.get(url)
      rescue Exception => e
        puts "Exception: #{e}"
        return
      else
        agent.page.save(fname)
        sleep 5 + rand
      end
    end

    # Create an HTML model of the organization
    org = Nokogiri::HTML(File.open(fname))

    org_id = db[:orgs].insert(
      :name    => org.xpath("//span[@id='#{pfx}OrgNameLabel']").text,
      :street  => org.xpath("//span[@id='#{pfx}OrgAddress1Label']").text,
      :suite   => org.xpath("//span[@id='#{pfx}OrgAddress2Label']").text,
      :city    => org.xpath("//span[@id='#{pfx}OrgCityLabel']").text,
      :state   => org.xpath("//span[@id='#{pfx}OrgStateLabel']").text,
      :post    => org.xpath("//span[@id='#{pfx}OrgPostalCodeLabel']").text,
      :phone   => org.xpath("//span[@id='#{pfx}Label1']").text,
      :website => org.xpath("//span[@id='#{pfx}OrgWebsiteLabel']").text,
      :type    => org.xpath("//span[@id='#{pfx}OrgCategoryLabel']").text,
    )

    i = 0
    # Contact: First Name, Last Name, Title, Phone, E-mail
    org.xpath("//table[@id='ctl00_PublicContent_DataList1']/tr").each do |c|
      fmt = "%02d" % i

      db[:contacts].insert(
        :fname  => c.xpath("td/span[@id='#{dtl}#{fmt}_ConfInd1FirstName']").text.strip,
        :lname  => c.xpath("td/span[@id='#{dtl}#{fmt}_ConfInd1LastName']").text,
        :title  => c.xpath("td/span[@id='#{dtl}#{fmt}_ConfInd1Title']").text,
        :phone  => c.xpath("td/span[@id='#{dtl}#{fmt}_ConfInd1Phone']").text,
        :email  => c.xpath("td/span[@id='#{dtl}#{fmt}_ConfInd1Email']").text,
        :org_id => org_id
      )

      i = i + 1
    end
  end
end

#
def site_scrape(agent, db)
  base = "http://www.scholarshipproviders.org"
  fname = "data/nspa_membership_directory.html"
  url = "#{base}/Content/ContentDisplay.aspx?ContentID=54"

  unless File.exists?(fname)
    begin
      # puts "Downloading Membership Directory: #{url}" # DEBUG
      agent.get(url)
    rescue Exception => e
      puts "Exception: #{e}"
      return
    else
      agent.page.save(fname)
      sleep 2 + rand
    end
  end

  # Create a document model of the HTML file
  doc = Nokogiri::HTML(File.open(fname))

  # Retrieve all the subdirectory links in alphabetical order
  doc.xpath("//a").each do |a|
    next if a['href'].nil?
    next if not a['href'].include? '/secure/membership_directory.aspx?Alpha='

    fname = "data/nspa_#{a['href'].sub(/\[/, '').sub(/\]/, '').sub(/.*\=/, '')}.html"
    url = "#{base}#{a['href'].sub(/[.][.]/, '')}"

    unless File.exists?(fname)
      begin
        # puts "Downloading Membership Listing Page: #{url}" # DEBUG
        agent.get(url)
      rescue Exception => e
        puts "Exception: #{e}"
        return
      else
        agent.page.save(fname)
        sleep 3 + rand
      end
    end

    # Scrape nspa member data from the directory listing page
    site_scrape_member(agent, db, fname)

    # break # DEBUG
  end # doc.xpath()

end

# Create an agent for crawling the web
agent = Mechanize.new { |a|
  a.user_agent_alias = 'Mac Safari'
  a.follow_meta_refresh = true
}

# Instantiate a DB instance for data persistence
# Database repository initialization
DB = Sequel.sqlite # memory database

DB.create_table :orgs do
  primary_key :id
  String      :name, :unique => true, :null => false
  String      :street
  String      :suite
  String      :city
  String      :state
  String      :post
  String      :phone
  String      :website
  String      :type
end

DB.create_table :contacts do
  primary_key :id
  String      :fname
  String      :lname
  String      :title
  String      :phone
  String      :email
  foreign_key :org_id, :orgs # This contact belongs to an organization
end

# Attempt to login to the web site
if not site_login(agent, 'blacksl55amg', 'shaner31')
  puts "Unable to log in."
  exit(true)
end

site_scrape(agent, DB)

# Log out after processing website information
logout = agent.page.forms[0].button_with(
               :name => 'ctl00$Header$LoginView2$LoginStatus1$ctl01')
agent.page.forms[0].click_button(logout)

# Save the contents of the database to a CSV File
org_csv = CSV.open("data/OrgIndex.csv", "wb")
org_csv << [ "Name", "Address", "City", "State", "Zip", "Phone", "Website", "Type" ]

entry = []
DB[:orgs].each do |o|
  entry << o[:name]
  if not o[:suite].empty?
    entry << "#{o[:street]}, #{o[:suite]}"
  else
    entry << "#{o[:street]}"
  end
  entry << o[:city]
  entry << o[:state]
  entry << o[:post]
  entry << o[:phone]
  entry << o[:website]
  entry << o[:type]
  DB[:contacts].where(:org_id => o[:id]).each do |c|
    entry << c[:fname]
    entry << c[:lname]
    entry << c[:title]
    entry << c[:phone]
    entry << c[:email]
  end

  org_csv << entry
  entry.clear
end

# End of file
